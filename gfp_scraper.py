import os
import requests
from bs4 import BeautifulSoup


def download_pdfs(url):
    response = requests.get(url)
    response.encoding = 'utf-8'

    soup = BeautifulSoup(response.text, 'lxml')

    if not os.path.exists('pdfs'):
        os.makedirs('pdfs')

    data = {}

    rows = soup.find_all('tr')

    for row in rows[1:]:
        number = row.find('td', class_='number').text

        name = row.select('td.number + td')[0].text

        link = row.find('a', href=True)
        url = link['href'] if link else None

        data[number] = {'name': name, 'url': url}
        file_id = url.split('/d/')[1].split('/')[0]
        download_url = f"https://drive.usercontent.google.com/u/0/uc?id={file_id}&export=download"
        file_response = requests.get(download_url)

        with open(os.path.join('pdfs', f'{data[number]["name"]}.pdf'), 'wb') as f:
            f.write(file_response.content)


if __name__ == '__main__':
    download_pdfs('http://gfp.kz/projectLicensec/projectStandarts/index.html')
